# nanobot interaction
def nanobot_setnick(id, args)
	# Parse args: Username email nickname
	args  = args.strip.split(/[\s]/, 3)
	name  = args[0]
	email = args[1]
	nick  = args[2]

	# Set the nick
	report(%x(setfattr -n user.nick -v "#{nick}" /home/#{name}))

	# Send user account creation notification
	mailbody = %{
Your nickname "#{nick}" has been set.
Nanobot will voice you next time you join #shells using this nick

Kind regards,
Cool Fire
}

	sendmail(email, mailbody)

	buildresult(id, 0, "User #{name} set nickname #{nick}")
end