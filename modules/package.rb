# Apt-style package management
def package_install_request(id, args)
	# Parse args: Username email_address package
	args = args.split(/ /, 3)

	# Install package
	result = package_install(id, args[2])

	# Notify user
	  # Send user notification
  mailbody = %{
Your package install request  for package '#{args[2]}' was processed, here is the result:

#{result}

If there were errors please check if your package name is correct:
https://packages.debian.org/search?suite=stretch&keywords=#{args[2]}

Kind regards,
Cool Fire
}
	sendmail(args[1], mailbody)
	return result
end

def package_install(id, package)
	cmd = "apt-get update && apt-get -y install #{package}"
	res = execute(cmd)

	message = ""
	if(res)
		message = "Package #{package} installed"
	else
		message = "Package #{package} install failed"
	end
	buildresult(id, res, message)
end

def package_remove(id, package)
	cmd = "apt-get -y remove #{package}"
	res = execute(cmd)

	message = ""
	if(res)
		message = "Package #{package} removed"
	else
		message = "Package #{package} remove failed"
	end
	buildresult(id, res, message)
end

def package_purge(id, package)
	cmd = "apt-get -y purge #{package}"
	res = execute(cmd)

	message = ""
	if(res)
		message = "Package #{package} purged"
	else
		message = "Package #{package} purge failed"
	end
	buildresult(id, res, message)
end

def package_upgrade(id, package)
	cmd = "apt-get update"
	res = execute(cmd)
	if(res)
		cmd = "apt-get -y upgrade"
		res = execute(cmd)
	end

	message = ""
	if(res)
		message = "Updates installed"
	else
		message = "Update failed"
	end
	buildresult(id, res, message)
end
