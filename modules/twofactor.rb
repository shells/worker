# Manage Two-factor auth
def twofactor_google_enable(id, args)
  args    = args.strip.split(/[\s]/, 3)
  name    = args[0]
  email   = args[1]

  # Add user to group
  report(%x(usermod -a -G twofactor-google #{name}))

  # Send user notification
  mailbody = %{
You are now using google two factor authentication.
Run the 'google-authenticator' command to configure your token if you have not already done so.

Kind regards,
Cool Fire
}

  sendmail(email, mailbody)

  buildresult(id, 0, "Google authenticator for #{name} enabled")
end

def twofactor_google_disable(id, args)
  args    = args.strip.split(/[\s]/, 3)
  name    = args[0]
  email   = args[1]

  # Remove user from group
  report(%x(deluser #{name} twofactor-google))
 
  # Send user notification
  mailbody = %{
You have STOPPED using google two factor authentication.

Kind regards,
Cool Fire
}

  sendmail(email, mailbody)

  buildresult(id, 0, "Google authenticator for #{name} disabled")
end