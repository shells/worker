# Postgres database stuff
def postgres_request(id, args)
	postgres_create_user(id, args)
end

def postgres_request_extra(id, args)
	postgres_create_db(id, args)
end

def postgres_create_user(id, args)
	args    = args.strip.split(/[\s]/, 3)
	user    = args[0].downcase
	email   = args[1]
	dbname  = "#{user}_#{args[2]}".downcase

	charset = ['a','b','d','e','f','h','k','m','n','q','r','s','t','y','z',
		       'A','B','D','E','F','G','J','K','M','N','R','S','T','Y','Z',
		       '2','3','4','6','7','8']
	
	# Generate password
	pass    = (0..12).map { charset[rand(charset.length)] }.join

	# Create roles
	report(%x(psql -U root -d template1 -c "CREATE ROLE #{dbname} NOSUPERUSER NOCREATEDB NOCREATEROLE NOINHERIT NOLOGIN;"))
	report(%x(psql -U root -d template1 -c "CREATE ROLE #{user} NOSUPERUSER NOCREATEDB NOCREATEROLE NOINHERIT LOGIN ENCRYPTED PASSWORD '#{pass}';"))
	report(%x(psql -U root -d template1 -c "GRANT #{dbname} TO #{user};"))

	# Create database
	report(%x(psql -U root -d template1 -c "CREATE DATABASE #{dbname} WITH OWNER=#{user};"))

	# Revoke public access to database
	report(%x(psql -U root -d template1 -c "REVOKE ALL ON DATABASE #{dbname} FROM public;"))

	# Allow table creation
	report(%x(psql -U root -d #{dbname} -c "GRANT ALL ON SCHEMA public TO #{user} WITH GRANT OPTION;"))

	# Send user account creation notification
	mailbody = %{
Your PostgreSQL request has been approved.
Username: #{user}
Password: #{pass}
Database: #{dbname}

Kind regards,
Cool Fire
}

	sendmail(email, mailbody)
	buildresult(id, 0, "Postgres db #{dbname} created")
end

def postgres_create_db(id, args)
	args    = args.strip.split(/[\s]/, 3)
	user    = args[0].downcase
	email   = args[1]
	dbname  = "#{user}_#{args[2]}".downcase

	# Create role
	report(%x(psql -U root -d template1 -c "CREATE ROLE #{dbname} NOSUPERUSER NOCREATEDB NOCREATEROLE NOINHERIT NOLOGIN;"))
	report(%x(psql -U root -d template1 -c "GRANT #{dbname} TO #{user};"))

	# Create database
	report(%x(psql -U root -d template1 -c "CREATE DATABASE #{dbname} WITH OWNER=#{user};"))

	# Revoke public access to database
	report(%x(psql -U root -d template1 -c "REVOKE ALL ON DATABASE #{dbname} FROM public;"))

	# Allow table creation
	report(%x(psql -U root -d #{dbname} -c "GRANT ALL ON SCHEMA public TO #{user} WITH GRANT OPTION;"))

	# Send out notification
	mailbody = %{
Your PostgreSQL database request has been approved.
Database name: #{dbname}

Kind regards,
Cool Fire
}

	sendmail(email, mailbody)
	buildresult(id, 0, "Postgres db #{dbname} created")
end
