# raise limits
def memory_limit_request(id, args)
	args    = args.strip.split(/[\s]/, 3)
	user    = args[0]
	email   = args[1]
	comment = args[2]

	# Add user to group
	add_user_to_group(user, 'raised-mem')

	# Send notification
	mailbody = %{
You have been added to the "raised-mem" group, his means you can run use up to 7.5 GB of RAM.
Please be careful!

If you are logged in at the time of receiving this email you will need to log out first before the new limits become active.
If you need limits raised higher, talk to a staff member.

Kind regards,
Cool Fire
}

	sendmail(email, mailbody)
	buildresult(id, 0, "User #{user} added to raised-mem")
end

def process_limit_request(id, args)
	args    = args.strip.split(/[\s]/, 3)
	user    = args[0]
	email   = args[1]
	comment = args[2]

	# Add user to group
	add_user_to_group(name, 'raised-proc')

	# Send notification
	mailbody = %{
You have been added to the "raised-proc" group, his means you can run up to 250 processes.
Please be careful!

If you are logged in at the time of receiving this email you will need to log out first before the new limits become active.
If you need limits raised higher, talk to a staff member.

Kind regards,
Cool Fire
}

	sendmail(email, mailbody)
	buildresult(id, 0, "User #{user} added to raised-proc")
end
