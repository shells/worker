# MySQL database stuff
def mysql_request(id, args)
	mysql_create_user(id, args)
end

def mysql_request_extra(id, args)
	mysql_create_db(id, args)
end

def mysql_create_user(id, args)
	args    = args.strip.split(/[\s]/, 3)
	user    = args[0]
	email   = args[1]
	dbname  = "#{user}_#{args[2]}"

	# Check for chars that should not be in database names
	if( dbname !~ /^([0-9a-zA-Z$_])*$/ )
		return buildresult(id, false, "MySQL db #{dbname} contains illegal characters.")
	end

	charset = ['a','b','d','e','f','h','k','m','n','q','r','s','t','y','z',
		       'A','B','D','E','F','G','J','K','M','N','R','S','T','Y','Z',
		       '2','3','4','6','7','8']
	
	# Generate password
	pass    = (0..12).map { charset[rand(charset.length)] }.join

	# Read MySQL pass
	rootpass = File.readlines('config/mysql.pass')[0].strip
	
	# Create database
	report(%x(mysqladmin -u root -p#{rootpass} create #{dbname}))

	# Grant privileges on database
	report(%x(mysql -u root -p#{rootpass} -e "GRANT ALL PRIVILEGES ON #{dbname}.* TO '#{user}'@'%' IDENTIFIED BY '#{pass}'"))
	
	# Send user account creation notification
	mailbody = %{
Your MySQL request has been approved.
Username: #{user}
Password: #{pass}
Database: #{dbname}

If you need it, phpmyadmin can be found here:
https://www.insomnia247.nl/phpmyadmin/

Kind regards,
Cool Fire
}

	sendmail(email, mailbody)
	buildresult(id, 0, "MySQL db #{dbname} created")
end

def mysql_create_db(id, args)
	args    = args.strip.split(/[\s]/, 3)
	user    = args[0]
	email   = args[1]
	dbname  = "#{user}_#{args[2]}"

	# Check for chars that should not be in database names
	if( dbname !~ /^([0-9a-zA-Z$_])*$/ )
		return buildresult(id, false, "MySQL db #{dbname} contains illegal characters.")
	end

	# Read MySQL pass
	rootpass = File.readlines('config/mysql.pass')[0].strip
	
	# Create database
	report(%x(mysqladmin -u root -p#{rootpass} create #{dbname}))

	# Grant privileges on database
	report(%x(mysql -u root -p#{rootpass} -e "GRANT ALL PRIVILEGES ON #{dbname}.* TO '#{user}'@'%'"))
	
	mailbody = %{
Your MySQL database request has been approved.
Database name: #{dbname}

If you need it, phpmyadmin can be found here:
https://www.insomnia247.nl/phpmyadmin/

Kind regards,
Cool Fire
}

	sendmail(email, mailbody)
	buildresult(id, 0, "MySQL db #{dbname} created")
end

