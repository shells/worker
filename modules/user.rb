# User mangement
def check_credits
	client = MessageBird::Client.new(@config['messagebird_access_key'])
	return client.balance.amount > 0
end

def sendsms(number, message, user)
	begin
		client = MessageBird::Client.new(@config['messagebird_access_key'])
		client.message_create('Insomnia247', number, message, :reference => "password_#{user}")
	rescue Exception => e
		report("Failed to send SMS: #{e.to_s}")
		return false
	end
	return true
end

def get_ldap_session
	ldap_server   = @config['ldap_server']
	ldap_admin    = @config['ldap_admin']
	ldap_password = File.read('/etc/pam_ldap.secret').chomp

	ldap = Net::LDAP.new(
	  :host => ldap_server,
	  :port => 389,
	  :auth => {
	    :method   => :simple,
	    :username => "#{ldap_admin},#{@ldap_base}",
	    :password => ldap_password,
	  }
	)
end

def add_user_to_group(name, group)
	ldap = get_ldap_session

	if ldap.search(
	  :base          => "#{@ldap_oug},#{@ldap_base}",
	  :filter        => "cn=#{group}",
	  :attributes    => ['memberuid'],
	  :return_result => true
	).first['memberuid'].include? name
		report("User #{name} is already in #{group} group.")
	else
		# User is not in the group, go ahead and add it
	  if ldap.modify(
	    :dn         => "cn=#{group},#{@ldap_oug},#{@ldap_base}",
	    :operations => [
	      [:add, :memberuid, name]
	    ]
	  )
	    report("Added #{name} to #{group} group.")
	  else
	    report("Failed to add #{name} to #{group} group.")
	    report(ldap.get_operation_result)
	  end
	end
end

def remove_user_from_group(name, group)
	ldap = get_ldap_session

	if ldap.search(
	  :base          => "#{@ldap_oug},#{@ldap_base}",
	  :filter        => "cn=#{group}",
	  :attributes    => ['memberuid'],
	  :return_result => true
	).first['memberuid'].include? name

		# User is in the group, go ahead and remove it
	  if ldap.modify(
	    :dn         => "cn=#{group},#{@ldap_oug},#{@ldap_base}",
	    :operations => [
	      [:delete, :memberuid, name]
	    ]
	  )
	    report("Removed #{name} from #{group} group.")
	  else
	    report("Failed to remove #{name} from #{group} group.")
	    report(ldap.get_operation_result)
	  end
	else
	  report("User #{name} is already not in #{group} group.")
	end
end

def user_create(id, args)
	# Parse args: Username email_address
	args    = args.strip.split(/[\s]/, 3)
	name    = args[0]
	namewww = "#{name}-www"
	email   = args[1]
	number  = args[2]

	# Check if user does not exist already
	if Dir.exist?("/home/#{name}")
		return buildresult(id, false, "User #{name} already exists.")
	end

	# Create session with LDAP server
	ldap = get_ldap_session

	# Query used group id's from LDAP server
	gids = []
	ldap.search(
	  :base          => "#{@ldap_oug},#{@ldap_base}",
	  :filter        => 'gidnumber=*',
	  :attributes    => ['gidnumber'],
	  :return_result => false
	) do |entry|
	  gids.push entry['gidnumber'].first.to_i
	end

	# Find next available group id
	newgid = 1000
	while gids.include? newgid do
	  newgid += 1
	end

	# Create new LDAP group object
	grpdn    = "cn=#{name},#{@ldap_oug},#{@ldap_base}"
	grpattrs = {
	  :cn           => name,
	  :objectclass  => ['posixGroup','top'],
	  :userpassword => '{crypt}x',
	  :gidnumber    => "#{newgid}",
	}

	# Shove it into the LDAP server
	if ldap.add( :dn => grpdn, :attributes => grpattrs )
	  report("Group #{name} added with gid #{newgid}.")
	else
	  report("Failed to add group #{name} with gid #{newgid}!")
	  report(ldap.get_operation_result)
	end

	# Query used user id's from LDAP server
	uids = []
	ldap.search(
	  :base          => "#{@ldap_oup},#{@ldap_base}",
	  :filter        => 'uidnumber=*',
	  :attributes    => ['uidnumber'],
	  :return_result => false
	) do |entry|
	  uids.push entry['uidnumber'].first.to_i
	end

	# Find next available user id
	newuid = 1000
	while uids.include? newuid do
	  newuid += 1
	end

	# Generate new password hash
	charset = ['a','b','d','e','f','h','k','m','n','q','r','s','t','y','z',
	         'A','B','D','E','F','G','J','K','M','N','R','S','T','Y','Z',
	         '2','3','4','6','7','8']

	pass    = (0..12).map { charset[rand(charset.length)] }.join
	salt    = (0..4).map { charset[rand(charset.length)] }.join

	pwhash  = pass.crypt("$6$#{salt}")

	# Create new LDAP user object
	usrdn   = "uid=#{name},#{@ldap_oup},#{@ldap_base}"
	usrattrs = {
	  :cn               => name,
	  :gidnumber        => "#{newgid}",
	  :homedirectory    => "/home/#{name}",
	  :loginshell       => '/bin/bash',
	  :objectclass      => ['account', 'posixAccount', 'top', 'shadowAccount' ],
	  :shadowlastchange => '0',
	  :shadowmax        => '99999',
	  :shadowwarning    => '7',
	  :uid              => name,
	  :uidnumber        => "#{newuid}",
	  :userpassword     => "{crypt}#{pwhash}",
	}

	# Shove it into the LDAP server
	if ldap.add( :dn => usrdn, :attributes => usrattrs )
	  report("User #{name} added with uid #{newuid}.")
	else
	  report("Failed to add user #{name} with uid #{newuid}!")
	  report(ldap.get_operation_result)
	end

	# Query used user id's from LDAP server
	wuids = []
	ldap.search(
	  :base          => "#{@ldap_oup},#{@ldap_base}",
	  :filter        => 'uidnumber=*',
	  :attributes    => ['uidnumber'],
	  :return_result => false
	) do |entry|
	  wuids.push entry['uidnumber'].first.to_i
	end

	# Find next available user id
	newwuid = 1000
	while wuids.include? newwuid do
	  newwuid += 1
	end

	# Create new LDAP user object
	usrwdn    = "uid=#{name}-www,#{@ldap_oup},#{@ldap_base}"
	usrwattrs = {
	  :cn               => "#{name}-www",
	  :gidnumber        => "#{newgid}",
	  :homedirectory    => "/home/#{name}-www",
	  :loginshell       => '/bin/false',
	  :objectclass      => ['account', 'posixAccount', 'top', 'shadowAccount' ],
	  :shadowlastchange => '15000',
	  :shadowmax        => '99999',
	  :shadowwarning    => '7',
	  :uid              => "#{name}-www",
	  :uidnumber        => "#{newwuid}",
	  :userpassword     => '{crypt}!',
	}

	# Shove it into the LDAP server
	if ldap.add( :dn => usrwdn, :attributes => usrwattrs )
	  report("User #{name}-www added with uid #{newwuid}.")
	else
	  report("Failed to add user #{name}-www with uid #{newwuid}!")
	  report(ldap.get_operation_result)
	end

	# Add new user to 'users' group
	usersgrpdn  = "cn=users,#{@ldap_oug},#{@ldap_base}"
	usersgrpops = [
	  [:add, :memberuid, "#{name}"]
	]

	if ldap.modify( :dn => usersgrpdn, :operations => usersgrpops )
	  report("Added #{name} to group 'users'.")
	else
	  report("Could not add #{name} to group 'users'!")
	  report(ldap.get_operation_result)
	end

	# Create home directory
	report(%x(mkdir /home/#{name}))
	report(%x(mkdir /home/#{name}/public_html))

	# Create .bash_history
	report(%x(touch /home/#{name}/.bash_history))

	# Ensure directory permissions
	report(%x(chown -R #{name}:#{name} /home/#{name}))
	report(%x(chmod -R 750 /home/#{name}))

	# Create login script directory
	report(%x(mkdir /etc/login.d/#{name}))
	report(%x(chmod 755 /etc/login.d/#{name}))

	# Lock .bash_history
	report(%x(chattr +a /home/#{name}/.bash_history))

	# Write apache config
	create_vhost(name, "#{name}.insomnia247.nl", "/home/#{name}/public_html")
	default_page(name, "#{name}.insomnia247.nl", "/home/#{name}/public_html")

	# Enable vhost
	enable_vhost("user_#{name}_#{name}.insomnia247.nl.conf")

	# Reload Apache config
	reload_config

	# Add suwww config to sudoers
	sudoline = "#{name}\t\tALL = NOPASSWD: /bin/su #{namewww} -s /bin/bash\n"
	File.open("/etc/sudoers.d/suwww_#{name}", 'w') { |file| file.write(sudoline) }

	# Add user to mail config
	maillines = "#{name}@insomnia247.nl #{name}\n#{namewww}@insomnia247.nl #{name}"
	File.open("/etc/postfix/virtual.d/user_#{name}", 'w') { |file| file.write(maillines) }

	# Make postfix hashmap
	report(%x(awk 'FNR==1{print ""}1' /etc/postfix/virtual.d/* > /etc/postfix/virtual))
	report(%x(/usr/sbin/postmap /etc/postfix/virtual))

	# Reload postfix
	report(%x(service postfix reload))

	# Update invite database
	report(%x(ruby /root/new/update_db.rb #{name}))

	# Send user account creation notification
	mailbody = %{
Your shell request has been approved.
You can log in at insomnia247.nl with the following username:
Username: #{name}

Your password will be sent via SMS to the phone number you provided. If this
fails for any reason, you will receive an email with the password instead.

If you have any questions or comments you can contact us:
On IRC  : irc.insomnia247.nl in #shells
By email: coolfire@insomnia247.nl

More information can be found at http://wiki.insomnia247.nl/wiki/Shells

Kind regards,
Cool Fire
}

	sendmail(email, mailbody)

	if check_credits
		if sendsms(number, "Welcome to Insomnia 24/7! Your password is: #{pass}", name)
			report('Sent password by SMS')
		else
			report('Failed to send SMS, falling back to email.')
			sendmail(email, "Welcome to Insomnia 24/7! Your password is: #{pass}")
		end
	else
		report('No credits to send SMS, falling back to email.')
		sendmail(email, "Welcome to Insomnia 24/7! Your password is: #{pass}")
	end

	buildresult(id, 0, "User #{name} created")
end

def user_delete(id, args)
	# Args: Username
	name    = args.strip
	namewww = "#{name}-www"

	if name.nil? or name.empty? or name == ""
		report("Sometime is terribly wrong, we seem to be trying to remove an empty name. BAILING OUT NOW!")
		exit 1
	end

	# Get ldap session
	ldap = get_ldap_session

	# Remove user from groups
	remove_user_from_group(name, 'power-users')
	remove_user_from_group(name, 'raised-proc')
	remove_user_from_group(name, 'raised-mem')
	remove_user_from_group(name, 'users')

	# Remove www-user
	if ldap.delete(:dn => "cn=#{namewww},#{@ldap_oup},#{@ldap_base}")
	  report("Removed user #{namewww} from LDAP.")
	else
	  report("Failed to remove user #{namewww}!")
	  report(ldap.get_operation_result)
	end

	# Unlock .bash_history
	report(%x(chattr -a /home/#{name}/.bash_history))

	# Remove regular user
	if ldap.delete(:dn => "cn=#{name},#{@ldap_oup},#{@ldap_base}")
	  report("Removed user #{name} from LDAP.")
	else
	  report("Failed to remove user #{name}!")
	  report(ldap.get_operation_result)
	end

	# Remove group
	if ldap.delete(:dn => "cn=#{name},#{@ldap_oug},#{@ldap_base}")
	  report("Removed group #{name} from LDAP.")
	else
	  report("Failed to remove group #{name}!")
	  report(ldap.get_operation_result)
	end

	# Remove home directory
	report(%x(rm -rf /home/#{name}/))

	# Remove login scripts
	report(%x(rm -rf /etc/login.d/#{name}))

	# Disable vhost
	disable_vhost("user_#{name}_#{name}.insomnia247.nl.conf")

	# Remove apache config
	remove_vhost(name, "#{name}.insomnia247.nl")

	# Reload Apache config
	reload_config

	# Remove suwww config
	report(%x(rm -rf /etc/sudoers.d/suwww_#{name}))

	# Remove mail config
	report(%x(rm -rf /etc/postfix/virtual.d/user_#{name}))

	# Make postfix hashmap
	report(%x(awk 'FNR==1{print ""}1' /etc/postfix/virtual.d/* > /etc/postfix/virtual))
	report(%x(/usr/sbin/postmap /etc/postfix/virtual))

	# Reload postfix
	report(%x(service postfix reload))

	buildresult(id, 0, "User #{name} deleted")
end
