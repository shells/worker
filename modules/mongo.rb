# mongodb stuff
def mongo_request(id, args)
	mongo_create_user(id, args)
end

def mongo_create_user(id, args)
	args    = args.strip.split(/[\s]/, 3)
	user    = args[0]
	email   = args[1]
	dbname  = "#{user}_#{args[2]}"

	charset = ['a','b','d','e','f','h','k','m','n','q','r','s','t','y','z',
		       'A','B','D','E','F','G','J','K','M','N','R','S','T','Y','Z',
		       '2','3','4','6','7','8']
	
	# Generate password
	pass    = (0..12).map { charset[rand(charset.length)] }.join

	# Read mongodb pass
	rootpass = File.readlines('config/mongo.pass')[0].strip
	
	# Create database
	cmd = "mongo -u root -p#{rootpass} admin --eval \"db.getSiblingDB('#{dbname}').createUser( { user: '#{user}', pwd: '#{pass}', roles: [ {'role':'readWrite', 'db':'#{dbname}'} ] } )\""
	res = execute(cmd)
	
	if(res)
		# Send user account creation notification
		mailbody = %{
Your mongodb request has been approved.
Username: #{user}
Password: #{pass}
Database: #{dbname}

Kind regards,
Cool Fire
}

		sendmail(email, mailbody)
		buildresult(id, 0, "Mongo db #{dbname} created")
	else
		buildresult(id, 1, "Mongo db #{dbname} creation failed!")
	end
end
