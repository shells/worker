# Execute command
def execute(cmd)
	system("#{cmd} >/dev/null 2>&1")
end

# Build result JSON
def buildresult(id, exitcode, message)
    result_message = JSON.generate({
        :id        => id,
        :timestamp => Time.now.to_i,
        :exitcode  => exitcode ? 0 : 1,
        :message   => message
    })
end

# Command to send mail upon completing requests
def sendmail(email, body)
	%x(echo '#{body}' | mail -s 'Request completed.' #{email})
end