# Manage Apache config
def httpd_public_html_enable(id, args)
  args    = args.strip.split(/[\s]/, 3)
  name    = args[0]
  email   = args[1]

  enable_vhost("user_#{name}_#{name}.insomnia247.nl.conf")
  reload_config

  # Send user notification
  mailbody = %{
Your public_html directory was enabled. You should be able to find your site at:
#{name}.insomnia247.nl

Kind regards,
Cool Fire
}

  sendmail(email, mailbody)

  buildresult(id, 0, "Public_html for #{name} enabled")
end

def httpd_public_html_disable(id, args)
  args    = args.strip.split(/[\s]/, 3)
  name    = args[0]
  email   = args[1]

  disable_vhost("user_#{name}_#{name}.insomnia247.nl.conf")
  reload_config
  
  # Send user notification
  mailbody = %{
Your public_html directory was disabled.
#{name}.insomnia247.nl should no longer be available.

Kind regards,
Cool Fire
}

  sendmail(email, mailbody)

  buildresult(id, 0, "Public_html for #{name} disabled")
end

def httpd_logs_enable(id, args)
  args    = args.strip.split(/[\s]/, 3)
  name    = args[0]
  email   = args[1]
  domain  = "#{name}.insomnia247.nl"
  dir     = "/home/#{name}/public_html"

  # Write new config
  create_vhost(name, domain, dir, true)
  reload_config

  # Send user notification
  mailbody = %{
Your http logs have now been enabled.

Logs are located here:
/home/#{name}/Apache_#{domain}_access.log
/home/#{name}/Apache_#{domain}_error.log

Kind regards,
Cool Fire
}

  buildresult(id, 0, "HTTP logs for #{name} enabled")
end

def httpd_logs_disable(id, args)
  args    = args.strip.split(/[\s]/, 3)
  name    = args[0]
  email   = args[1]
  domain  = "#{name}.insomnia247.nl"
  dir     = "/home/#{name}/public_html"

  # Write new config
  create_vhost(name, domain, dir, false)
  reload_config

  # Send user notification
  mailbody = %{
Your http logs have now been disabled.
Existing log files will not be updated anymore.

Kind regards,
Cool Fire
}

  buildresult(id, 0, "HTTP logs for #{name} disabled")
end

def httpd_vhost_request(id, args)
	httpd_add_vhost(id, args)
end

def httpd_add_vhost(id, args)
	args    = args.strip.split(/[\s]/, 4)
	name    = args[0]
	email   = args[1]
	domain  = args[2]
	dir     = args[3]
	
	# Check for blank or faulty dirs
	if( dir.nil? )
		dir = "/home/#{name}/public_html"
	end
	if( dir !~ /^\/home\/#{name}\// )
		if( dir =~ /^~\// )
			dir.gsub!( /^~\//, "/home/#{name}/" )
		elsif( dir =~ /^\$HOME\// )
			dir.gsub!( /^\$HOME\//, "/home/#{name}/" )
		elsif( dir =~ /^\// )
			dir = "/home/#{name}#{dir}"
		else
			dir = "/home/#{name}/#{dir}"
		end
	end
	if( dir =~ /\/$/ )
		dir.gsub!( /\/$/, "" )
	end

	# Create config
	create_vhost(name, "#{domain}", "#{dir}")

	# Enable config
	enable_vhost("user_#{name}_#{domain}.conf")
	reload_config

	# Send user notification
	mailbody = %{
Your domain request for #{domain} has been approved.
Make sure you point your DNS A record for this domain to our servers.

Kind regards,
Cool Fire
}

	sendmail(email, mailbody)

	buildresult(id, 0, "VHost #{domain} created")
end

def create_vhost(user, domain, directory, logs=true)
  
  logdirective = ''
  if( logs == true )
    logdirective = %{
  ErrorLog        /home/#{user}/Apache_#{domain}_error.log
  CustomLog       /home/#{user}/Apache_#{domain}_access.log common
}
  end

	vhost = %{
<VirtualHost *:80>
  ServerName      #{domain}
  ServerAlias     www.#{domain}

  DocumentRoot    #{directory}/

  <Directory #{directory}>
          AllowOverride All
          Options MultiViews Indexes SymLinksIfOwnerMatch IncludesNoExec

          <Limit GET POST OPTIONS>
                  Require all granted
          </Limit>
          <LimitExcept GET POST OPTIONS>
                  Require all denied
          </LimitExcept>
  </Directory>

  <Directory #{directory}/cgi-bin>
          AllowOverride FileInfo AuthConfig Limit Indexes
          Options -MultiViews +SymLinksIfOwnerMatch +ExecCGI
          SetHandler cgi-script

          <Limit GET POST OPTIONS>
                Require all granted
          </Limit>
          <LimitExcept GET POST OPTIONS>
                Require all denied
          </LimitExcept>
  </Directory>

  <IfModule mpm_itk_module>
          AssignUserId #{user}-www #{user}
  </IfModule>

  BandWidthModule On
  ForceBandWidthModule On
  BandWidth all 400000

#{logdirective}
</VirtualHost>
}

	File.open("/etc/apache2/sites-available/user_#{user}_#{domain}.conf", 'w') { |file| file.write(vhost) }

  # Write logrotate rules for these logs too
  logrotatecommand = %{
/home/#{user}/Apache_#{domain}_access.log {
  monthly
  missingok
  rotate 3
  compress
  notifempty
  create 640 #{user} #{user}
  sharedscripts
  postrotate
    /etc/init.d/apache2 reload > /dev/null
  endscript  
}

/home/#{user}/Apache_#{domain}_error.log {
  monthly
  missingok
  rotate 3
  compress
  notifempty
  create 640 #{user} #{user}
  sharedscripts
  postrotate
    /etc/init.d/apache2 reload > /dev/null
  endscript
}
  }

  File.open("/etc/logrotate.d/user_#{user}_#{domain}.conf", 'w') { |file| file.write(logrotatecommand) }

end

def remove_vhost(user, domain)
	report(%x(rm /etc/apache2/sites-available/user_#{user}_#{domain}.conf))
  report(%x(rm /etc/logrotate.d/user_#{user}_#{domain}.conf))
end

def enable_vhost(name)
	report(%x(a2ensite #{name}))
end

def disable_vhost(name)
	report(%x(a2dissite #{name}))
end

def reload_config
	report(%x(service apache2 reload))
end

def default_page(user, domain, directory)
	page = %{
<html>
  <head>
    <title>Default page - Insomnia 24/7</title>
  </head>
  <body>
    <p>
      <h1>It works!</h1>
    </p>
    <p>
      If you are seeing this page your website is working. This is the default page for #{domain}.
    </p>
  </body>
</html>
	}

	File.open("#{directory}/index.html", 'w') { |file| file.write(page) }

	report(%x(chown #{user}:#{user} #{directory}/index.html))
	report(%x(chmod 660 #{directory}/index.html))
end