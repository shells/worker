#!/usr/bin/env ruby
# encoding: utf-8

require 'bunny'
require 'json'
require 'messagebird'
require 'net/ldap'
require 'openssl'
require 'optparse'
require 'syslog'
require 'time'

Dir["modules/*.rb"].each {|file| load file }

# Function to report errors (to syslog or to stderr)
def report(message)
    if(@daemonize)
        Syslog.open('worker_daemon', Syslog::LOG_PID | Syslog::LOG_CONS) { |s| s.info message }
    else
        warn(message)
    end
end

# Variable declarations
last_id = -1
@daemonize = false
options = {}
@config = {}

# Parse commandline options
OptionParser.new do |opts|
    # Alternate configuration file
    opts.on('-c file', '--config file', String, 'Specify config file. (Default: config/rabbitmq.conf') do |o|
        options[:config] = o
    end

    # Daemonize option
    opts.on('-d', '--daemonize', 'Run in background') do |o|
        options[:daemon] = o
    end
end.parse!

# Default config file if no alternate is specified
if(options[:config].nil?)
    options[:config] = 'config/rabbitmq.conf'
end

# Read config
if(File.exists?(options[:config]))
    configdata = ''
    File.open(options[:config]) do |file|
        file.each do |line|
            configdata << line
        end
    end

    # Parse config
    @config = JSON.parse(configdata)
else
    raise "Config file does not exist: #{options[:config]}"
end


# Start up RabbitMQ
conn = Bunny.new(
                    :host     => @config['host'],
                    :user     => @config['user'],
                    :password => @config['pass'],
                )
conn.start

ch   = conn.create_channel
rx   = ch.queue(@config['channel_receive'])
tx   = ch.queue(@config['channel_send'])

# LDAP constants
@ldap_base = @config['ldap_base']
@ldap_oup  = @config['ldap_oup']
@ldap_oug  = @config['ldap_oug']

# Read public key so we can decrypt messages
public_key = OpenSSL::PKey::RSA.new(File.read(@config['public_keyfile']))


# Check if we should be daemonized
if(options[:daemon] || @config['daemonize'])
    puts "Worker running in background."
    @daemonize = true
    Process.daemon(true)
end


# Start waiting for messages
rx.subscribe(:block => true) do |delivery_info, properties, payload|

    # Try to decrypt and parse message
    begin
        message = JSON.parse(public_key.public_decrypt(payload))

        # Check if the message seems to be authentic and sane
        # and attempt to execute it.
        if(Time.now.to_i > message['timestamp'] + @config['max_skew'])
            report("Rejected message #{message['id']}: Timestamp too old. (#{delivery_info[:channel].connection.host})")
        elsif(last_id >= message['id'])
            report("Rejected message #{message['id']}: ID does not make sense. (#{delivery_info[:channel].connection.host})")
        else
            # Update last seen id
            last_id = message['id']

            # Run command
            result = eval("#{message['command']}(#{message['id']}, \"#{message['args']}\")")

            # Send result back
            string = public_key.public_encrypt(result)
            ch.default_exchange.publish(string, :routing_key => tx.name)
        end
    rescue NoMethodError => e
        report("Rejected message #{message['id']}: Invalid command '#{message['command']}'. (#{delivery_info[:channel].connection.host})")
        report(e.to_s)
    rescue Exception => e
        report("Could not decode message. (#{delivery_info[:channel].connection.host})")
        report(e.to_s)
    end
end

conn.close
